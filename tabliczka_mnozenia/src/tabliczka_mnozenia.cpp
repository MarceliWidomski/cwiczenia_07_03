#include <iostream>
using namespace std;

int main() {// wypisuje tabliczke mnozenia
	int zakres;
	cout << "Podaj najwiekszy czynnik jaki ma sie pojawic w tabliczce mnozenia: ";
	cin >> zakres;
	for (int i = 1; i <= zakres; ++i) {
		for (int j = 1; j <= zakres; ++j) {
		cout << j << " * "<< i << " = " << j*i << "\t";
	}
	cout << endl;
}
return 0;
}
