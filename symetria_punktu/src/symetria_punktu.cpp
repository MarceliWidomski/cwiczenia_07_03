//oblicza symetrie wzgledem punktu

#include <iostream>
#include <string>
using namespace std;

int main() {
	string nazwa1("A");
	string nazwa2("S");
	string nazwa3("B");
	int opcja;
	int x1(0);
	int y1(0);
	int x2(0);
	int y2(0);
	int x3(0);
	int y3(0);
	while (opcja != 4) {
		cout << "Menu:" << endl;
		cout << "1) Wpisz punkt ktorego obraz chcesz utworzyc" << endl;
		cout << "2) Wpisz punkt symetrii" << endl;
		cout << "3) Oblicz punkt wspolrzedne punktu symetrycznego" << endl;
		cout << "4) Zakoncz" << endl;
		cout << "Wybierz numer opcji: " << endl;
		cin >> opcja;

		switch (opcja) {
		case 1: {
			cout << "Podaj nazwe punktu: ";
			cin >> nazwa1;
			cout << "Podaj wspolrzedna X punktu " << nazwa1 << " (X" << nazwa1
					<< "): ";
			cin >> x1;
			cout << "Podaj wspolrzedna Y punktu " << nazwa1 << " (Y" << nazwa1
					<< "): ";
			cin >> y1;
			cout << endl;
			cout << "Punkt: " << nazwa1 << " (" << x1 << ", " << y1 << ")"
					<< endl;
			cout << endl;
			break;
		}
		case 2: {
			cout << "Podaj nazwe punktu symetrii: ";
			cin >> nazwa2;
			cout << "Podaj wspolrzedna X punktu " << nazwa2 << " (X" << nazwa2
					<< "): ";
			cin >> x2;
			cout << "Podaj wspolrzedna Y punktu " << nazwa2 << " (Y" << nazwa2
					<< "): ";
			cin >> y2;
			cout << endl;
			cout << "Punkt symetrii: " << nazwa2 << " (" << x2 << ", " << y2
					<< ")" << endl;
			cout << endl;
			break;
		}
		case 3: {
			cout << "Podaj nazwe obrazu punktu " << nazwa1 << ": ";
			cin >> nazwa3;
			x3 = 2 * x2 - x1;
			y3 = 2 * y2 - y1;
			cout << endl;
			cout << "Obraz punktu " << nazwa1 << "(" << x1 << ", " << y1 << ")"
					<< " wzgledem punktu " << nazwa2 << "(" << x2 << ", " << y2
					<< ")" << " to punkt " << nazwa3 << "(" << x3 << ", " << y3
					<< ")" << endl;
			cout << endl;
			break;
		}
		case 4: {
			cout << "Wylaczam...";
			break;
		}
		default: {
			cout << "Niewlasciwy wybor opcji. Sprobuj ponownie." << endl;
		}
		}
	}
	return 0;
}
