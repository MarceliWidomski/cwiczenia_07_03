#include <iostream>
using namespace std;

int main() {//wypisuje potegi dwojki(przesuniecie bitowe)
	cout << "Podaj najwyzszy wykladnik potegi: " << endl;
	int liczba;
	cin >> liczba;
	int wynik(1);

	for (int i = 0; i <= liczba; ++i, wynik = wynik << 1)
		cout << "2 ^ " << i << " = " << wynik << endl;
	return 0;
}
